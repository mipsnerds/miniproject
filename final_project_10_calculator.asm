 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 # 			POCKET CALCULATOR 
 #
 #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 # 			Initialize
 #
 # 			Led's address
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.eqv SEVENSEG_LEFT 0xFFFF0010 # Dia chi cua den led 7 doan trai.
.eqv SEVENSEG_RIGHT 0xFFFF0011 # Dia chi cua den led 7 doan phai 
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 # 			Keyboard's in/out address
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.eqv IN_ADRESS_HEXA_KEYBOARD 0xFFFF0012
.eqv OUT_ADRESS_HEXA_KEYBOARD 0xFFFF0014
 
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 # 			Encode number => led display
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.data
	encode:		.word	0x3F, 0x6, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F
	
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 			MAIN Procedure
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.text
main:
 #---------------------------------------------------------
 # 			Initialize 
 #	s0	pressed button's code
 #	s1	number display on left led (-1 : off)
 #	s2	number display on right led (-1 : off)
 #	s3	First term of the statement (-1 : not assigned)
 #	s4	Second term of the statement (-1: not assigned)
 #	s5	Operator's code (-1: not assigned)
 #	s6	Result of the statement (-1: not assigned)
 #---------------------------------------------------------	
init: 
	li	$s0, -1
	li	$s1, -1
	li	$s2, -1
	li	$s3, -1
	li	$s4, -1
	li	$s5, -1
	li	$s6, -1				
	la	$s7, encode
	li	$a1, 0

 #---------------------------------------------------------
 # 	Enable interrupts you expect
 #---------------------------------------------------------
 #	Enable the interrupt of Keyboard matrix 4x4 of Digital Lab
	li	$t1, IN_ADRESS_HEXA_KEYBOARD
 	li 	$t3, 0x80 					# bit 7 = 1 to enable
	sb 	$t3, 0($t1)
	
 #---------------------------------------------------------
 # Loop an print sequence numbers
 #---------------------------------------------------------
Loop: 
	nop
	nop
 	nop
 	nop
 	nop
sleep: 
	addi 	$v0,$zero,32 					
 	li 	$a0,200 					
 	syscall
 	nop 		
b Loop

end_main:

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 	GENERAL INTERRUPT SERVED ROUTINE for all interrupts
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ktext 0x80000180

IntSR: 

 #--------------------------------------------------------
 # 		Temporary disable interrupt
 #-------------------------------------------------------- 
get_cod:
	li 	$t1, IN_ADRESS_HEXA_KEYBOARD
	li 	$t2, OUT_ADRESS_HEXA_KEYBOARD

#--------------------------------------------------------
# 	Check row 1 and re-enable bit 7
#--------------------------------------------------------	
 	li 	$t3, 0x81 						
 	sb	$t3, 0($t1) 						
 	
	lb 	$a0, 0($t2)
	bne	$a0, $0, get_key
#--------------------------------------------------------
# 	Check row 2 and re-enable bit 7
#--------------------------------------------------------	
	li 	$t3, 0x82 					
 	sb	$t3, 0($t1)
 	 						
	lb 	$a0, 0($t2)	
	bne	$a0, $0, get_key
#--------------------------------------------------------
# 	Check row 3 and re-enable bit 7
#--------------------------------------------------------	
	li 	$t3, 0x84 					
 	sb	$t3, 0($t1) 						
 	
	lb 	$a0, 0($t2)
	bne	$a0, $0, get_key
#--------------------------------------------------------
# 	Check row 4 and re-enable bit 7
#--------------------------------------------------------	
	li 	$t3, 0x88 				
 	sb	$t3, 0($t1) 	
 						
	lb 	$a0, 0($t2)
	bne	$a0, $0, get_key

#--------------------------------------------------------
# Convert from key code in $a0 to number or operator, stored in $s0
#
# @param[in]	$a0
#
# Changed	$a0
#
# @param[out]   $s0
#-------------------------------------------------------- 
get_key:
	andi	$a0, $a0, 0xFF
	
	beq	$a0, 0x11, number_0
	beq	$a0, 0x21, number_1
	beq	$a0, 0x41, number_2
	beq	$a0, 0x81, number_3
	beq	$a0, 0x12, number_4
	beq	$a0, 0x22, number_5
	beq	$a0, 0x42, number_6
	beq	$a0, 0x82, number_7
	beq	$a0, 0x14, number_8
	beq	$a0, 0x24, number_9
	beq	$a0, 0x44, number_a
	beq	$a0, 0x84, number_b
	beq	$a0, 0x18, number_c
	beq	$a0, 0x28, number_d
	beq	$a0, 0x48, number_e
	beq	$a0, 0x88, number_f
number_0:
	addi 	$s0, $0, 0
	b	process_key
number_1:
	addi 	$s0, $0, 1
	b	process_key
number_2:
	addi 	$s0, $0, 2
	b	process_key
number_3:
	addi 	$s0, $0, 3
	b	process_key
number_4:
	addi 	$s0, $0, 4
	b	process_key
number_5:
	addi 	$s0, $0, 5
	b	process_key
number_6:
	addi 	$s0, $0, 6
	b	process_key
number_7:
	addi 	$s0, $0, 7
	b	process_key
number_8:
	addi 	$s0, $0, 8
	b	process_key
number_9:
	addi 	$s0, $0, 9
	b	process_key
number_a:
	addi 	$s0, $0, 10
	b	process_key	
number_b:
	addi 	$s0, $0, 11
	b	process_key	
number_c:
	addi 	$s0, $0, 12
	b	process_key	
number_d:
	addi 	$s0, $0, 13
	b	process_key	
number_e:
	addi 	$s0, $0, 14
	b	process_key	
number_f:
	addi 	$s0, $0, 15
	b	process_key	

 #--------------------------------------------------------
 # 		Evaluate the key pressed
 #
 # @param[in]	$s0
 # 
 #--------------------------------------------------------
process_key:
	blt	$s0, 10, term
	b	operator
	
term:
	bltz	$s5, term1
	b	term2
	
term1:
 #--------------------------------------------------------
 #		Evaluate the first term
 #--------------------------------------------------------
pre_cal_con:
 #--------------------------------------------------------
 # If $s6 >= 0 then start a new statement 
	bnez	$a1, new_cal		
	
 #--------------------------------------------------------
 # $s6 < 0: If $s3 >= 0 then combine the term from previous number
	bgez	$s3, sum_term1
	
 #--------------------------------------------------------	
 # $s6 < 0 and $s3 < 0 : fresh new statement
	b 	else
new_cal:
	li	$s3, -1
	li	$s6, -1
	li	$s1, -1
	li	$s2, -1
	li	$a1, 0
	jal 	OFF_LED
	b	else
	
 #--------------------------------------------------------
 # 	Combine the term from previous number
 #--------------------------------------------------------
else:
	add	$s3, $s0, $0			# s3 = s0
	b	display

sum_term1:
	mul 	$s3, $s3, 10
	add	$s3, $s3, $s0
	b	display
 #--------------------------------------------------------
 #		Display the term
 #--------------------------------------------------------
display:
	bgez	$s2, char1			# s2 >= 0 -> xu ly charled 1
	
char2: 
	add	$s2, $s0, $0
	b	led_display
char1:
	add	$s1, $s2, $0
	add	$s2, $s0, $0
	b 	led_display
	
 #--------------------------------------------------------
 # 		Display the led with values stored in $s1, $s2
 #
 # @param[in]	$s1, $s2
 # 
 # Changed	$t4, $t5, $a0
 #--------------------------------------------------------	
led_display:
led_right:
	bltz	$s2, end_process
	mul	$t4, $s2, 4
	add	$t5, $s7, $t4
	lw	$a0, 0($t5)
	jal	SHOW_7SEG_LEFT

led_left:
	bltz	$s1, end_process
	beq	$s1, 10, negative_sign
	mul	$t4, $s1, 4
	add	$t5, $s7, $t4
	lw	$a0, 0($t5)
	b	show
	
negative_sign:
	li	$a0, 0x40
show:
	jal	SHOW_7SEG_RIGHT
	b 	end_process

 #--------------------------------------------------------
 # 		Evaluate the second term
 #
 # @param[in]	$s4
 # 
 # Changed	$s1, $s2, $s4
 #--------------------------------------------------------
term2:
	bgtz	$s4, sum_term2			
	
reload_led:
	jal	OFF_LED
	li	$s1, -1
	li 	$s2, -1

	add	$s4, $s0, $0		
	b	display

 #--------------------------------------------------------
 # 		Combine the term from previous number
 #--------------------------------------------------------
sum_term2:
	mul 	$s4, $s4, 10
	add	$s4, $s4, $s0
	b 	display
 #--------------------------------------------------------
 # 		Evaluate the operator
 # If operator = f then jump to calculate
 # If operator = e then jump to AC
 #
 # @param[in]	$s0
 #
 # Changed	$s5 
 #--------------------------------------------------------
operator:
	beq	$s0, 15, calculate
	beq	$s0, 14, AC
	add	$s5, $s0, $0
	b	end_process
	
 #--------------------------------------------------------
 # 			Reset
 #--------------------------------------------------------
AC:
	li	$s0, -1
	li	$s1, -1
	li	$s2, -1
	li	$s3, -1
	li	$s4, -1
	li	$s5, -1
	li	$s6, -1	
	jal 	OFF_LED
	b	end_process	
	
 #--------------------------------------------------------
 # 		Calculate
 #
 # @param[in]	$s3, $s4, $s5
 #
 # @param[out]	$s6
 #
 #--------------------------------------------------------
calculate:
	beq	$s5, 10, sum
	beq	$s5, 11, sub_
	beq	$s5, 12, multi
	beq	$s5, 13, divide

sum:
	add	$s6, $s3, $s4
	b	display_res
	
sub_:
	sub	$s6, $s3, $s4
	b	display_res
	
multi: 
	mul	$s6, $s3, $s4
	b	display_res
	
divide:
	div	$s6, $s3, $s4
	b	display_res

 #--------------------------------------------------------
 # 		Display the result
 #
 # @param[in]	$s6
 # 
 # Changed	$t4, $t5
 #
 # @param[out]	$s1, $s2
 
 #--------------------------------------------------------	
display_res:
	add	$t5, $s6, $0			# temp[4] = s6
	abs	$t5, $t5
	div	$t5, $t5, 10
	mfhi	$t4				# t4 la phan du
	add	$s2, $t4, $0
	
	bltz	$s6, neg_display
	div	$t5, $t5, 10
	mfhi	$t4
	add	$s1, $t4, $0
	b	reassign
neg_display:
	li	$s1, 10

 #--------------------------------------------------------
 # 	Reassign register for next calculation
 # Reset s4, s5 for term2, operator
 # Reassign s3 for term1 = last result = s6
 #--------------------------------------------------------		
reassign:
 	add	$s3, $s6, $0
 	li	$s4, -1
 	li	$s5, -1
 	bltz	$s6, neg_result
 	li	$a1, 1
 	b	led_display
 	
 neg_result:
	li	$a1, -1
	b	led_display
 #--------------------------------------------------------
 # 		End process
 #--------------------------------------------------------
end_process:
 	mtc0 	$zero, $13 				# Must clear cause reg
 	
 #--------------------------------------------------------
 # 	Evaluate the return address of main routine
 # epc <= epc + 4
 #--------------------------------------------------------
next_pc:
	mfc0 	$at, $14 			# $at <= Coproc0.$14 = Coproc0.epc
 	addi 	$at, $at, 4 			# $at = $at + 4 (next instruction)
 	mtc0 	$at, $14 				# Coproc0.$14 = Coproc0.epc <= $at 
return: 
	eret 					# Return from exception


#---------------------------------------------------------------
# 	SHOW_7SEG_LEFT : turn on/off the 7seg
# param[in] $a0 value to shown
# remark $t0 changed
#---------------------------------------------------------------
SHOW_7SEG_LEFT: 
	li $t0, SEVENSEG_LEFT # assign port's address
 	sb $a0, 0($t0) # assign new value
 	jr $ra
#---------------------------------------------------------------
# Function SHOW_7SEG_RIGHT : turn on/off the 7seg
# param[in] $a0 value to shown
# remark $t0 changed
#---------------------------------------------------------------
SHOW_7SEG_RIGHT: 
	li 	$t0, SEVENSEG_RIGHT # assign port's address
 	sb 	$a0, 0($t0) # assign new value
 	jr 	$ra 
#---------------------------------------------------------------
# Function OFF_LED : turn off the 7seg
# remark $t0 changed
#---------------------------------------------------------------
OFF_LED:
	li 	$t0, SEVENSEG_LEFT # assign port's address
	add	$t6, $0, $0
 	sb 	$t6, 0($t0) # assign new value
 	
 	li 	$t0, SEVENSEG_RIGHT # assign port's address
	add	$t6, $0, $0
 	sb 	$t6, 0($t0) # assign new value
 	jr 	$ra