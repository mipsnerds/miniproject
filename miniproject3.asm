# 				INSERTION SORT WITH .WORD ARRAY


#----------------------------------------------------------------------------------------------------#
.data
	A:	.word	6, 5, 3, 1, 8, 7, 2	# Array A to sort
	Aend:	.word				# Temp element for the address of last element of array A
	Message:.asciiz	"Array A after being sorted: "
	Space:	.ascii	" "
	
#----------------------------------------------------------------------------------------------------#
.text
main:
	la 	$a0, A				# Load the address of A[0] to a0
	la	$a1, Aend
	addi 	$a1, $a1, -4			# Load the address of last element A[n - 1] to a1
	la	$a2, A				# 
	seq	$t0, $a0, $a1			#
	bne	$t0, $0, print_result		# Neu a0 == a1, hay mang A chi co mot phan tu => nhay den print_result
	j	ins_sort			# Insertion Sort

done:	
	li	$v0, 10				# exit
	syscall
	
end_main:

#----------------------------------------------------------------------------------------------------#
# PROCEDURE 	print_result
# @brief	Print out array A
# @param[in]	a1		Dia chi phan tu cuoi cung cua mang A[n]
#		a0		Dia chi phan tu dau tien cua mang A[0]
# @param[out]	
# @note		
print_result:
	add	$a3, $a0, $zero			# a3 = a0
	li	$v0, 4
	la	$a0, Message
	syscall
	
	add	$a2, $a3, $zero			# a2 = a0
loop_print:
	bgt	$a2, $a1, end_loop_print
	li	$v0, 1
	lw	$a0, 0($a2)			# print the content of element that a2 point to
	syscall
	
	li	$v0, 4
	la	$a0, Space
	syscall
	
	addi	$a2, $a2, 4			# increase a2 to point to the next element 
	j	loop_print
	
end_loop_print:
	j	done

#----------------------------------------------------------------------------------------------------#
# PROCEDURE 	ins_sort
# @brief	Main sort procedure
# @param[in]	a2		Dia chi phan tu cuoi cung cua mang dang xet (A[k])
#		a1		Dia chi phan tu cuoi cung cua mang A		
# @param[out]	
# @note		
ins_sort:
	addi	$a2, $a2, 4			# a2 points to the next element
	
loop_ins_sort:
	bgt	$a2, $a1, done_ins_sort		# Branch to done if a2 > a1, or finish sorting by the last element !!!!!!!!!!!!!!!!!!!!!!
	j	insert
	
done_ins_sort:
	j	print_result
	
#----------------------------------------------------------------------------------------------------#
# PROCEDURE 	insert
# @brief	Insert element 
#
# @param[in]	a2		Dia chi phan tu cuoi cung cua mang dang xet (A[k])
#		
# @param[out]	
#
# @note		
#
insert:
	lw	$t0, 0($a2)			# t0 = value of a[k]
	addi 	$a3, $a2, -4			# a3 points to a[k - 1]
	
loop_insert:
	#condition
	blt	$a3, $a0, done_loop_insert	# break the loop if i < 1, or da duyet qua het cac phan tu tu k -> 1
	lw	$t1, 0($a3)			# t3 = value of a[i]
	bge	$t1, $t0, done_loop_insert	# break the loop if a[i] <= value
	
	#loop
	addi 	$t4, $a3, 4			# t4 = a[i + 1] address
	sw	$t1, 0($t4)			# a[i + 1] = a[i] !!!!!!!!!!!!!
	addi	$a3, $a3, -4			# i = i - 1
	j 	loop_insert
	
done_loop_insert:
	addi	$t4, $a3, 4
	sw	$t0, 0($t4)			# luu gia tri cua t1 vao bien co dia chi t4
	j	ins_sort
