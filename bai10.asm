# 				Project 10 - Power, Square, Hexadecimal

#----------------------------------------------------------------------------------------------------#
.data
	Message: 		.asciiz 	"Input an integer: "
	overflowed_alert: 	.asciiz 	"Overflowed"
	input: 			.space 		11
	hexa: 			.space 		10
	wrong_input: 		.asciiz 	"\nNhap sai\n"
	no_input: 		.asciiz 	"\nKhong co so duoc nhap\n"
	title: 			.asciiz 	"i\t\t\tpower(2,i)\t\tsquare(i)\t\tHexadecimal(i)\n"
	tabspace1: 		.asciiz 	"\t\t"
	tabspace2: 		.asciiz 	"\t\t\t"

.text
#----------------------------------------------------------------------------------------------------#
# PROCEDURE 	Get_Input
# @brief	Nhap n tu ban phim
# @param[in]	
# @param[out]	$a0	n
# @note		
Input: # Nhap mot so nguyen
Get_Input: 
	li 	$v0,4
	la 	$a0,Message
	syscall
	li 	$v0,8
	la 	$a0,input
	li 	$a1,11
	syscall
	nop

#----------------------------------------------------------------------------------------------------#
# PROCEDURE 	storage1
# @brief	Khoi tao cac bien va hang so se su dung
# @param[in]	
# @param[out]	$s1
#		$t0 = 0			bien count cho input loop	
#		$t1 = '0' - 1		
#		$t2 = '9' + 1
#		$t3 = '\n'
# @note	
storage1: 
	la 	$s0,input
	li 	$s1,0 			#$s1: so i se duoc convert tu string nhap vao, co gia tri tung ki tu cua s0
	li 	$t0,0 			#$t0 = 0(bien count cho input loop)
	li 	$t1,47 			#$t1 = '0' - 1
	li 	$t2,59 			#$t2 = '9' + 1
	li 	$t3,10 			#$t3 = '\n'(ky tu enter)
	nop
	
#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	loop
# @brief	Vong lap input
# @param[in]	
#		$a0			n		
# @param[out]	
#
# @note		su dung mult, mflo
#
loop: 
	add 	$a0,$s0,$t0 		#$a0 la bien chay, duyet xau s0
	lb 	$a1,0($a0)		#$a1 = input[i]
	beq 	$a1,$t3,check_input	#if input[i] = '\n' then check input
	beqz 	$a1,check_input		#if input[] end then check input => duyet het xau
	slt 	$a2,$t1,$a1		#$a2 = 1 if $a1 >= '0' else = 0 (t0 = '0')
	slt 	$a3,$a1,$t2		#$a3 = 1 if $a1 <= '9' else = 0 
	add 	$a2,$a2,$a3		
	bne 	$a2,2,wrong_print	#if $a2 != 2 then input[i] is not a number
	mult 	$s1,$t3			# input[i] is a number: set hi and lo to the product of s1 and t3: 
	mflo 	$s1			# $s1 = $s1 * 10 (gan s1 = lo)
	addi 	$t0,$t0,1		# t0: i = i + 1
	addi 	$a1,$a1,-48 		# else convert input[i] to number
	add 	$s1,$s1,$a1		# $s1 += $s1 + $a1
	j loop

check_input:
	beq 	$t0, $0, no_input_print #if string is null then print no input	
	j 	convert_to_number 	#else convert
	
#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	convert_to_number
# @brief	chuan bi thanh ghi s0, jump to Main
# @param[in]	
#		
# @param[out]	
#		$s0			so nguyen n
# @note		
#	
convert_to_number:
	add 	$s0,$s1,$0 		#$s0 = $s1
	j 	Main 			#jump to Main

#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	no_input_print + wrong_input
# @brief	In loi ra console
# @param[in]	
#		
# @param[out]	
#		
# @note		
#
no_input_print: #print for no input case
	li 	$v0, 4
	la 	$a0, no_input
	syscall
	j 	End_Main

wrong_print: #print for wrong input case
	li 	$v0, 4
	la 	$a0, wrong_input
	syscall
	j 	End_Main
	
#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	Main
# @brief	
# @param[in]	
#		
# @param[out]	
#		s1			power(2, i)
#		s2			square(i)
#		a1 = 2			dung de tinh 2^i
# @note		
#	
Main:
	li $s1,0 			#s1 = power(2,i)
	li $s2,0 			#s2 = square(i)
	li $a1,2 			#a1 = 2(dung de tinh 2^i)
	li $v0,4 
	la $a0, title
	syscall 			#in title

	jal print_i
	nop
	jal print_power
	nop 
	jal print_square
	nop
	jal print_hexadecimal
	nop
	j End_Main
	
#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	print_i
# @brief	In i ra console
# @param[in]	s0			i
#		
# @param[out]	
#		
# @note		
#
print_i: 
	li 	$v0,36			# In so nguyen khong dau thap phan
	add 	$a0,$0,$s0		# In s0
	syscall 			# print i
	
	addi 	$sp,$sp,-4		# Stack voi 1 .word
	sw 	$ra,4($sp)		# Luu dia chi procedure
	li 	$t3,10000000
	slt 	$t1,$s0,$t3		# t1 = 1 if s0 < t3
	beq 	$t1,0,print_tabspace1 	# if i>=10000000 then print tabspace1(dung de tao form cho table)
	li 	$v0,4
	la 	$a0,tabspace2 		#else print tabspace2(dung de tao form cho table)
	syscall
	
	addi 	$sp,$sp,4
	jr 	$ra 			#tro ve Main

#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	print_power
# @brief	In luy thua cua 2
# @param[in]	
#		
# @param[out]	s1 = 2^n
#		
# @note		
#		t1			output
#		t2 = s0 
#
print_power: #procedure to print power(2,i)
	li 	$t1,1 			#output = $t1 = 1
	add 	$t2,$0,$s0 		#$t2 = $s0 = i
	slti  	$t1,$t2,32
	addi 	$sp,$sp,-4
	sw 	$ra,4($sp)
	beq 	$t1,0,overflowed 	#if i>=32 then print for overflowed case
	sllv 	$s1,$t1,$s0 		#else $s1 = shift $t1 left i bit($s1 = 2^i)
	li 	$v0,36
	add 	$a0,$0,$s1
	syscall 			#print $s1
	slti 	$t1,$s0,24
	beq 	$t1,0,print_tabspace1 	#if i>=24 then print tabspace1(dung de tao form cho table)
	li 	$v0,4
	la 	$a0,tabspace2 		#else print tabspace2(dung de tao form cho table)
	syscall
	addi 	$sp,$sp,4
	jr 	$ra 			#tro ve Main

#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	print_square
# @brief	Tinh va in binh phuong cua i
# @param[in]	
#		
# @param[out]	s2 = i ^ 2
#		
# @note		
#		
#
print_square: #procedure to print square(i)
	mul 	$s2,$s0,$s0 		#$s2 = $s0 * $s0($s2 = i^2)
	addi 	$sp,$sp,-4
	sw 	$ra,4($sp)
	li 	$t2,46342
	slt 	$t1,$s0,$t2
	beq 	$t1,0,overflowed 	#if i>=46342 then print for overflowed case
	li 	$v0,36
	add 	$a0,$0,$s2
	syscall 			#else print $s2
	
	li 	$t3,10000000
	slt 	$t1,$s2,$t3
	beq 	$t1,0,print_tabspace1 	#if $s2>=10000000 then print tabspace1(dung de tao form cho table)
	li 	$v0,4
	la 	$a0,tabspace2
	syscall 			#else print tabspace2(dung de tao form cho table)
	
	addi 	$sp,$sp,4
	jr 	$ra 			#tro ve Main

#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	print_i
# @brief	In i ra console
# @param[in]	
#		
# @param[out]	
#		
# @note		
#
print_hexadecimal: #procedure to print hexadecimal(i)
	addi 	$sp,$sp,-4
	sw 	$ra,4($sp)
	
storage2: #khai bao mot so bien va hang so se su dung
	li 	$t1,58          
	la 	$t2,0xe0000000
	la $t4,0xc000000
	
	
	
	beq 	$s0,$zero,print_zero 	#if $s0=0 then print for "zero" case
	li 	$t6,0 			#$t6: bien stopping count cho MaskAndShift loop
	li 	$t5,0 			#$t5: bien the hien viec da co ky tu duoc in($t5=0 -> chua co ky tu nao duoc in sau "0x")
	
#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	MaskAndShift
# @brief	
# @param[in]	
#		
# @param[out]	
#		
# @note		
#
and $t3,$t4,$s0
srl $t3,$t3,30
sll $s0,$s0,2
addi $t3,$t3,48
beq $t3,48,case_zero
addi $t5,$t5,1
b print

MaskAndShift:
	beq 	$t6,11,End_Main 		#if $t6=11 then end main
	and 	$t3,$t2,$s0 		#$t3 = $t2 & $s0($t3 = i & 0xf0000000)
	srl 	$t3,$t3,29  		#tach 4 bit dau cua i dua ve lam 4 bit cuoi cua $t3($t3 se co gia tri = gia tri 4 bit dau cua $s0)      
	sll 	$s0,$s0,3   		#dich i sang trai 4 bit(de tiep tuc vong lap voi 4 bit tiep theo tu trai sang)                        
	addi 	$t3,$t3,48 		#convert $t3 thanh so hexa(trong truong hop $t3<10) 
	beq 	$t3,48,case_zero 	#if $t3=0(4 bit dau cua $s0 co gia tri la 0 thi chay chuong trinh con "zero" case    
	addi 	$t5,$t5,1  		#if $t3=!0 -> da co ky tu duoc in
	blt 	$t3,$t1,print 		#if $t3<=9 -> in t3    
	addi 	$t3,$t3,7 		#if $t3>9 -> convert $t3 ra so hexa(trong truong hop $t3>9)           
	b 	print 			#in $t3
	
#in t3
print: 
	move 	$a0,$t3                 
	li 	$v0,11                  
	syscall 			#in $t3
	
	addi 	$t6,$t6,1 		#tang bien stopping count len 1
	b 	MaskAndShift 		#tro ve vong lap

case_zero: #truong hop $t3=0("zero" case)
	addi $t6,$t6,1 #tang bien stopping count len 1
	beq $t5,$0,MaskAndShift #if $t5=0(chua co ky tu nao duoc in) then tro ve vong lap
	li $v0,1 
	li $a0,0
	syscall #else print "0"
	
	b MaskAndShift #va tro ve vong lap

print_zero: #print "0x0" cho truong hop $s0=0
	li $v0,1
	li $a0,0
	syscall
	
	j End_Main #va ket thuc
         
#ket thuc chuong trinh
End_Main: 
	li $v0,10
	syscall
	
#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	print_tabspace1
# @brief	
# @param[in]	
#		
# @param[out]	
#		
# @note		
#
print_tabspace1:
	li $v0,4
	la $a0,tabspace1
	syscall
	
	lw $ra,4($sp)
	addi $sp,$sp,4
	jr $ra

#----------------------------------------------------------------------------------------------------#	
# PROCEDURE 	overflowed
# @brief	In overflow
# @param[in]	
#		
# @param[out]	
#		
# @note		
#
overflowed: 
	li 	$v0,4
	la 	$a0,overflowed_alert
	syscall 			#print "overflowed" message
	
	li 	$v0,4
	la 	$a0,tabspace1
	syscall
	
	lw 	$ra,4($sp)
	addi 	$sp,$sp,4
	jr 	$ra 			#va tro ve main
